import firebase from "firebase/app"
import "firebase/firestore"
import "firebase/auth"
import {firebaseConfig} from '../config/firekeys'

// Your web app's Firebase configuration

// Initialize Firebase
firebase.initializeApp(firebaseConfig)

export const db = firebase.firestore()
export const auth = firebase.auth()
export const googleAuth = new firebase.auth.GoogleAuthProvider()