import Router from 'koa-router';

import { getRecipes } from '../controllers/recipeController'

var router = Router({prefix: '/recipes'});

export default router
.get('/', getRecipes);