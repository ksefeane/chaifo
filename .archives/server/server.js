//create Koa app
import Koa from 'koa';
const app = new Koa();
const port = 3000;

//set up cors middleware
import cors from '@koa/cors';
app.use(cors());

//Set up body parsing middleware
import bodyParser from 'koa-body';
app.use(bodyParser({
    extended: false,
    formidable:{uploadDir: './uploads'},
    multipart: true,
    urlencoded: true
}));
//routes

//var user = require('./routes/userRoute.js');
import user from './routes/userRoute.js';
//import recipe from './routes/recipeRoute';
//import friend from './routes/friendRoute';
app.use(user.routes()).use(user.allowedMethods());
//app.use(recipe.routes())
//app.use(friend.routes());



app.listen(port, () => {
    console.log(`server listening on port ${port}...`);
});
