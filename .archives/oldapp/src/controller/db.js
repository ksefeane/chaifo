import {db, auth} from "../model/firebase";
import {collectionData} from "rxfire/firestore";

export function post(col, data) {
    return new Promise((yes, no) => {
        db.collection(col).add(data)
        .then(res => yes(res)).catch(e => {no(e)});
    })
}

export function target(col) {
    return db.collection(col);
}

