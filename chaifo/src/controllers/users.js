import {addDoc, getDoc, setDoc, watchDoc} from '../models/api'

export const createUser = (user) => {
  let data = {
    "uid": user.uid, 
    "name": user.displayName,
    "username": '',
    "email": user.email,
    "bio": '',
    "image": user.photoURL,
    "ingredients": [],
    "following": [],
    "followers": [],
    "blocked": []
  }
  return new Promise((yes, no) => {
    addDoc("users", data).then(r => {yes(data)}).catch(e => {no(e)})
  })
}

export const updateUser = (id, data) => {
  return new Promise((yes, no) => {
    setDoc("users", id, data).then(r => {yes(r)}).catch(e => {no(e)})
  })
}

export const getUser = (uid) => {
  let query = {
    'field': 'uid',
    'value': uid
  }
  return new Promise((yes, no) => {
    getDoc("users", query).then(snap => {
      let user = null
      snap.forEach(doc => {
        user = doc.data()
        user.docId = doc.id
      })
      yes(user)
    }).catch(e => {no(e)})
  })
}

export const watchUser = (uid) => {
  let query = {
    'field': 'uid',
    'value': uid
  }
  return watchDoc("users", query)
}

export const addIngredient = (user, data) => {
  let payload = {
    'ingredients': Array.from(user.ingredients)
  }
  payload.ingredients.push(data)
  return new Promise((yes, no) => {
    setDoc("users", user.docId, payload).then(r => {yes(r)}).catch(e => {no(e)})
  })
}