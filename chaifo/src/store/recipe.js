import {post, collect} from '../models/api'
import {firestoreAction} from 'vuexfire'

export default {
  namespaced: true,
    state: {
      recipes: []
    },
    mutations: {

    },
    actions: {
      addRecipe: (ctx, data) => {
        post("recipes", data).catch(e => {console.log(e)})
      },
      getRecipes: firestoreAction(ctx => {
        return ctx.bindFirestoreRef("recipes", collect("recipes")
        .orderBy("createdAt", "asc"))
      })
    }
}