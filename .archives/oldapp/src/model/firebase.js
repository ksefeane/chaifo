import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAwu_H5m0T6MuDWCA2T5KHCaiSP7f5R5KU",
    authDomain: "chaifo.firebaseapp.com",
    projectId: "chaifo",
    storageBucket: "chaifo.appspot.com",
    messagingSenderId: "1083244853419",
    appId: "1:1083244853419:web:82cbeb71f7e626e8065fee",
    measurementId: "G-1FZ8BCQEMF"
  };
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


export const db = firebase.firestore();
export const auth = firebase.auth();
export const googleAuth = new firebase.auth.GoogleAuthProvider();