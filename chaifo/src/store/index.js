import Vue from 'vue'
import Vuex from 'vuex'
import persist from 'vuex-persistedstate'
import {vuexfireMutations} from 'vuexfire'

import user from './user'
import recipe from './recipe'

Vue.use(Vuex)

export default new Vuex.Store({
  strict:true,
  mutations: vuexfireMutations,
  modules: {
    user: user,
    recipe: recipe
  },
  plugins: [persist()]
})
