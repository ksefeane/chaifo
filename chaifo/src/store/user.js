import {auth, googleAuth, db} from '../models/firebase'
import {post} from '../models/api'
import {addIngredient, createUser, getUser, updateUser, watchUser} from '../controllers/users'
import { firestoreAction } from 'vuexfire'

export default {
  namespaced: true,
    state: {
      logged: false,
      profile: null,
      userData: {}
    },
    mutations: {
      loginState: (state, val) => {state.logged = val},
      setProfile: (state, val) => {state.profile = val},
      removeIngredient: (state, val) => {
        let i = state.ingredients.indexOf(val)
        state.ingredients.splice(i, 1)
      },
      setUserData: (state, val) => {state.userData = val}
    },
    actions: {
      login: (ctx) => {
        auth.signInWithRedirect(googleAuth)
        auth.onAuthStateChanged(user => {
          ctx.commit('loginState', user ? true : false)
          ctx.commit('setProfile', user ? user : {})
        })
      },
      logout: (ctx) => {
        auth.signOut()
        auth.onAuthStateChanged(user => {
          ctx.commit('loginState', status)
          ctx.commit('setProfile', null)
        })
      },
      addIngredient: (ctx, val) => {
        addIngredient(ctx.state.userData, val)
        .then(() => {ctx.dispatch('getUser')}).catch(e => {console.log(e)})
      },
      removeIngredient: (ctx, val) => {
        ctx.commit('removeIngredient', val)
      },
      getUser: firestoreAction(ctx => {
        return ctx.bindFirestoreRef("userData", watchUser(ctx.state.profile.uid))
      }),
        // getUser(ctx.state.profile.uid).then(user => {
        //   user ? ctx.commit('setUserData', user) : ctx.dispatch('createUserData')
        // }).catch(e => {console.log(e)})
      // },
      createUserData: (ctx) => {
        createUser(ctx.state.profile)
        .then((r) => {ctx.commit('setUserData', r)}).catch(e => {console.log(e)})
      },
      postUserData: (ctx, data) => {
        updateUser(ctx.state.userData.docId, data)
        .then(() => {ctx.dispatch('getUser')})
        .catch(e => {console.log(e)})
      },
      getProfile: (ctx, data) => {
        ctx.commit('setProfile', data)
      }
    }
}