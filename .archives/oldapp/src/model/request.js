import axios from "axios";

export function get(path) {
    return new Promise((yes, no) => {
        axios({
            method: 'get',
            url: path
        }).then(res => {yes(res)}).catch(e => {no(e)})
    })
}

export function post(path, data) {
    return new Promise((yes, no) => {
        axios({
            method: 'post',
            url: path,
            data: data
        }).then(res => {yes(res)}).catch(e => {no(e)})
    })
}