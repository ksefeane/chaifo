var profile = {
    "username": "koriStrange",
    "bio": "I love cooking"
}

export async function userProfile(ctx, next) {
    ctx.body = profile;
    next();
}
