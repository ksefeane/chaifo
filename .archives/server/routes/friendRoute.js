import Router from 'koa-router';

import { friendProfile } from '../controllers/friendController'

var router = Router({prefix: '/f'});

export default router
.get('/', friendProfile);