import {Observable} from 'rxjs'
import {db} from './firebase'


export const get = (name) => {
  return new Observable(subscriber => {
    db.collection(name).get().then(snap => {
      snap.forEach(doc => {
        let data = {
          'id': doc.id,
          'name': doc.data().name,
          'username': doc.data().username,
          'createdAt': doc.data().createdAt
        }
        subscriber.next(data)
      })
    })
  })
}

export const post = (name, data) => {
  return new Promise((yes, no) => {
    db.collection(name).add(data)
    .then(res => {yes(res)}).catch(e => {no(e)})
  })
}

export const collect = (name) => {
  return db.collection(name)
}

export const postMatch = (col, match, data) => {
  return new Promise((yes, no) => {
    db.collection(col, '==', match).add(data).then(res => {yes(res)})
    .catch(e => {no(e)})
  })
}

export const getMatch = (col, match) => {
  return new Promise((yes, no) => {
    db.collection(col).where(match.field, '==', match.value).get()
    .then(res => {yes(res)}).catch(e => {no(e)})
  })
}

export const updateMatch = (col, id, data) => {
  return new Promise((yes, no) => {
    db.collection(col).doc(id).set(data, {merge: true})
    .then(res => {yes(res)}).catch(e => {no(e)})
  })
}

//controller apis
//=======================================================================

export const addDoc = (col, data) => {
  return new Promise((yes, no) => {
    db.collection(col).add(data)
    .then(res => {yes(res)}).catch(e => {no(e)})
  })
}

export const setDoc = (col, id, data) => {
  return new Promise((yes, no) => {
    db.collection(col).doc(id).set(data, {merge: true})
    .then(res => {yes(res)}).catch(e => {no(e)})
  })
}

export const getDoc = (col, data) => {
  return new Promise((yes, no) => {
    db.collection(col).where(data.field, '==', data.value).get()
    .then(r => {yes(r)}).catch(e => {no(e)})
  })
}

export const watchDoc = (col, data) => {
  return db.collection(col).where(data.field, '==', data.value)
}